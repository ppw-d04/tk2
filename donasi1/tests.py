from django.test import TestCase, Client
from .models import DonasiTunai
from .views import donasi_tunai, list_donasitunai

# Create your tests here.
class TestDonasiTunai(TestCase):
	def test_apakah_url_donasi_tunai_ada(self):
		response = Client().get('/donasi_tunai/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_form_donasi_tunai_ada_templatenya(self):
		response = Client().get('/donasi_tunai/')
		self.assertTemplateUsed(response, 'form_donasitunai.html')

	def test_apakah_ada_text_form_donasi_tunai_di_halamannya(self):
		response = Client().get('/donasi_tunai/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("Form Donasi Tunai", html_kembalian)

	def test_apakah_url_list_donatur_ada(self):
		response = Client().get('/donasi_tunai/list_donasitunai/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_list_donasi_tunai_ada_templatenya(self):
		response = Client().get('/donasi_tunai/list_donasitunai/')
		self.assertTemplateUsed(response, 'list_donasitunai.html')

	def test_apakah_ada_text_daftar_donatur_di_halamannya(self):
		response = Client().get('/donasi_tunai/list_donasitunai/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("Daftar Donatur", html_kembalian)

	def test_apakah_sudah_ada_modelsnya(self):
		DonasiTunai.objects.create(nama="Ayesha Indira", alamat="Jakarta", kontak="ayesha.indira.w@gmail.com", jumlah="Rp10.000", anon=False)
		hitung = DonasiTunai.objects.all().count()
		self.assertEqual(hitung, 1)
	
	def test_url_json(self):
		response = Client().get("/donasi_tunai/posts/?q=")
		self.assertEqual(response.status_code, 200)