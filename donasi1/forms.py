from django import forms
from .models import DonasiTunai

# Create your views here.
class DonasiTunaiForm(forms.ModelForm):
    class Meta:
        model = DonasiTunai
        fields = {'nama', 'alamat', 'kontak', 'jumlah', 'anon'}

        widgets = {
            'nama': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Nama Donatur',
                'required':True}),
            'alamat': forms.Textarea(attrs={
                'class':'form-control',
                'placeholder':'Alamat Donatur',
                'required':True}),
            'kontak': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'e.g.: contoh@email.com / 0213427253',
                'required':True}),
            'jumlah': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'e.g.: Rp10.000',
                'required':True}),
        }