$(document).ready(function() {
    $("#input").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        console.log(q)
        $.ajax({
            url: "data?q=" + q,
            success: function(data) {
                $('#konten').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].fields.nama + "</td>" +
                        "<td class='align-middle'>" + data.items[i].fields.jumlah + "</td>" 
                }
                $('#konten').append(result);
            },
        })
    });
});