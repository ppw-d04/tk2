$(document).ready(function(){
    $("input").focus(function(){
        $(this).css("background-color", "white");
    });

    $("input").blur(function(){
        $(this).css("background-color", "lightgrey");
    });

    $("button").click(function(){
        alert("Scroll keatas untuk melihat apakah berhasil");
    });
});

const alertBox = document.getElementById("alert-box")
const form = document.getElementById("myform")

const nama = document.getElementById("id_nama")
const kontak = document.getElementById("id_kontak")
const pertanyaan = document.getElementById("id_pertanyaan")

const csrf = document.getElementsByName("csrfmiddlewaretoken")
console.log(csrf)
const url = ""

const handleAlerts = (type, text)=>{
    alertBox.innerHTML = `<div class="alert alert-${type}" role="alert">${text}</div>`
}

form.addEventListener('submit', e=>{
    e.preventDefault()

    const fd = new FormData()
    fd.append('csrfmiddlewaretoken', csrf[0].value)
    fd.append('nama', nama.value)
    fd.append('kontak', kontak.value)
    fd.append('pertanyaan', pertanyaan.value)

    $.ajax({
        type: 'POST',
        url: url,
        enctype: false,
        data: fd,
        success: function(response){
            console.log(response)
            const sText = `successfully saved ${response.nama}`
            handleAlerts('success', sText)
            setTimeout(()=>{
                alertBox.innerHTML = ""
                nama.value = ""
                kontak.value =""
                pertanyaan.value=""
            }, 2000)
        },
        error: function(error){
            console.log(error)
            handleAlerts('danger','Something went wrong..')
        },
        cache: false,
        contentType: false,
        processData: false,
    })
})