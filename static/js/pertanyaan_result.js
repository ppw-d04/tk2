$(function() {
	$("#places").autocomplete({
	source: "/search/",
	minLength: 2,
	});
});

$("#places").on("keyup", function(e) {
	var q = $(this).val();
	$.ajax({
		url: "/search/?q=" + q,
		success: function(data) {
			console.log(data)
			$('.accordion').html('')
			var result= '';
		for (var i = 0; i < data.length; i++) {
			result += "<div class='accordion-content'>" + "<dt class='d-flex justify-content-between'>" + "<a><b>" + data[i].fields.pertanyaan + "</b></a>" + "</dt>" + 
			"<dd>" + "<p><b>Penulis : </b>" + data[i].fields.nama  + "</p>" + "</dd>" + "</div>"
		}
		$('.accordion').append(result);
		},
	})
});

$(document).ready(function(){
	$("#places").focus(function(){
		$(this).css("background-color", "white");
	});

	$("#places").blur(function(){
		$(this).css("background-color", "lightgrey");
	});

	$("#places").focusin(function(){
		alert("Bila tidak menemukan yang anda inginkan, silahkan mengisi form pertanyaan");
	});
});

(function($) {
    
  var allPanels = $('.accordion .accordion-content dd').hide();
    
  $('.accordion .accordion-content dt a').click(function() {
      $this = $(this);
      $target =  $this.parent().next();
      
      if($target.hasClass('active')){
        $target.removeClass('active').slideUp(); 
      }else{
        allPanels.removeClass('active').slideUp();
        $target.addClass('active').slideDown();
      }
      
    return false;
  });

})(jQuery);
