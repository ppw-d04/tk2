from django.db import models

# Create your models here.
class Pertanyaan(models.Model):
	nama = models.CharField('Nama', max_length=120)
	kontak = models.CharField('Email atau Nomor Handphone', max_length=120)
	pertanyaan = models.CharField('Pertanyaan', max_length=1000)