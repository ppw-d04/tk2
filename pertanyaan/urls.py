from django.urls import path
from . import views

app_name = 'pertanyaan'

urlpatterns = [
	path('asked_us/', views.asked_us, name='asked_us'),
	path('list_asked/', views.list_asked, name='list_asked'),
	path('search/', views.search, name='search')
]