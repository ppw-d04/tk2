from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from .forms import Form
from .models import Pertanyaan
from django.contrib.auth.decorators import login_required
import json

@login_required
def asked_us(request):
	form = Form(request.POST or None)
	data={}
	if request.is_ajax():
		if form.is_valid():
			form.save()
			data['nama'] = form.cleaned_data.get('nama')
			data['kontak'] =form.cleaned_data.get('kontak')
			data['pertanyaan'] =form.cleaned_data.get('pertanyaan')
			return JsonResponse(data)
	return render(request, 'form_pertanyaan.html', {'form' : form})

@login_required
def list_asked(request):
	mylist = Pertanyaan.objects.all()
	return render(request, 'list_result.html', {'mylist':mylist})

def search(request):
	q = request.GET.get('q')
	posts = Pertanyaan.objects.filter(pertanyaan__icontains = q)
	mylist = serializers.serialize('json', posts)
	return HttpResponse(mylist, content_type="text/json-comment-filtered")
