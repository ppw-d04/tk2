from django.test import TestCase, Client
from .views import asked_us, list_asked
from django.urls import reverse
from .models import Pertanyaan
from django.contrib.auth.models import User

class TestPertanyaan(TestCase):
	def test_apakah_url_asked_us_ada_sebelum_login(self):
		response = Client().get('/asked_us/')
		self.assertEquals(response.status_code, 302)

	def test_anonymous_cannot_see_page(self):
		response = self.client.get(reverse("pertanyaan:asked_us"))
		self.assertRedirects(response, "/accounts/login/?next=/asked_us/")

	def test_authenticated_user_can_see_page(self):
		user = User.objects.create_user("Juliana," "juliana@dev.io", "some_pass")
		self.client.force_login(user=user)
		response = self.client.get(reverse("pertanyaan:asked_us"))
		self.assertEqual(response.status_code, 200)

	def test_apakah_asked_us_ada_htmlnya(self):
		user = User.objects.create_user("Juliana," "juliana@dev.io", "some_pass")
		self.client.force_login(user=user)
		response = self.client.get(reverse("pertanyaan:asked_us"))
		self.assertTemplateUsed(response, 'form_pertanyaan.html')

	def test_apakah_models_add_activity_ada(self):
		Pertanyaan.objects.create(nama="abc", kontak="example@gmail.com", pertanyaan='Siapa penemu bumi?')
		self.assertEqual(Pertanyaan.objects.all().count(), 1)

	def test_ajax_post(self):
		user = User.objects.create_user("Juliana," "juliana@dev.io", "some_pass")
		self.client.force_login(user=user)
		response = self.client.get(reverse("pertanyaan:asked_us"))
		payload = {'nama': 'John Doe', 'kontak': '0815', 'pertanyaan':'test'}
		response = self.client.post('/asked_us/', payload, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
		self.assertEquals(response.status_code, 200)   

class TestList(TestCase):
	def test_apakah_url_list_asked_ada(self):
		response = Client().get('/list_asked/')
		self.assertEquals(response.status_code, 302)

	def test_authenticated_user_can_see_page(self):
		user = User.objects.create_user("Juliana," "juliana@dev.io", "some_pass")
		self.client.force_login(user=user)
		response = self.client.get(reverse("pertanyaan:list_asked"))
		self.assertEqual(response.status_code, 200)

	def test_apakah_list_asked_ada_htmlnya(self):
		user = User.objects.create_user("Juliana," "juliana@dev.io", "some_pass")
		self.client.force_login(user=user)
		response = self.client.get(reverse("pertanyaan:list_asked"))
		self.assertTemplateUsed(response, 'list_result.html')

	def test_search(self):
		response = Client().get("/search/?q=")
		self.assertEqual(response.status_code, 200)
