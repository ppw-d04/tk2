from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('infografis/', views.infografis, name='infografis'),
    path('faq/', views.faq, name='faq')
]
