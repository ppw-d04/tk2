from django.test import TestCase, Client
from .models import DonasiNonTunai
from .views import donasi_nontunai, list_donasinontunai

# Create your tests here.
class TestDonasiNonTunai(TestCase):
	def test_apakah_url_donasi_nontunai_ada(self):
		response = Client().get('/donasi_nontunai/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_form_donasi_nontunai_ada_templatenya(self):
		response = Client().get('/donasi_nontunai/')
		self.assertTemplateUsed(response, 'form_donasinontunai.html')

	def test_apakah_ada_text_form_donasi_nontunai_di_halamannya(self):
		response = Client().get('/donasi_nontunai/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("Form Donasi Non Tunai", html_kembalian)

	def test_apakah_url_list_donatur_ada(self):
		response = Client().get('/list_donasinontunai/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_list_donasi_nontunai_ada_templatenya(self):
		response = Client().get('/list_donasinontunai/')
		self.assertTemplateUsed(response, 'list_donasinontunai.html')

	def test_apakah_ada_text_daftar_donatur_di_halamannya(self):
		response = Client().get('/list_donasinontunai/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("Daftar Donatur", html_kembalian)
	
	def test_url_json(self):
		response = Client().get("/donasi_nontunai/posts/?q=")
		self.assertEqual(response.status_code, 200)

	def test_apakah_sudah_ada_modelsnya(self):
		DonasiNonTunai.objects.create(nama="Ghifari", alamat="BSD", kontak="ghifari@email.com", kebutuhan1="masker", kebutuhan2="hand sanitizer", anon=False)
		hitung = DonasiNonTunai.objects.all().count()
		self.assertEqual(hitung, 1)