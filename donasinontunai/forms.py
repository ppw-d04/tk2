from django import forms
from .models import DonasiNonTunai

class DonasiNonTunaiForm(forms.ModelForm):
    class Meta:
        model = DonasiNonTunai
        fields = '__all__'
        