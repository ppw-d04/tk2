from django.urls import path
from . import views

# Create your views here.
app_name = 'donasinontunai'

urlpatterns = [
    path('donasi_nontunai/', views.donasi_nontunai, name='donasi_nontunai'),
    path('list_donasinontunai/', views.list_donasinontunai, name='list_donasinontunai'),
    path('donasi_nontunai/posts/', views.posts, name='posts'),
]