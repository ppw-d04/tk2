from django.db import models

# Create your models here.
class DonasiNonTunai(models.Model):
	nama = models.CharField('Nama', max_length = 120)
	alamat = models.TextField('Alamat')
	kontak = models.CharField('Email atau Nomor Handphone', max_length = 120)
	kebutuhan1 = models.CharField('Kebutuhan Logistik', max_length = 120)
	kebutuhan2 = models.CharField('Kebutuhan Lainnya(Optional)', max_length = 120, blank=True, default='')
	anon = models.BooleanField('Tampilkan sebagai Donatur Anonim', default = False)
