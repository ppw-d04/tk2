from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

# Create your views here.
def login(request):
	if request.method == 'POST':
	    username = request.POST['username']
	    password = request.POST['password']
	    if User.objects.filter(username=username).exists():
		    user = authenticate(request, username=username, password=password)
		    if user is not None:
		        auth_login(request, user)
		        return redirect('/')
		    else:
		    	messages.error(request, 'Password Salah!')
	    else:
	    	messages.error(request, 'Akun belum terdaftar!')
	
	return render(request, 'login.html')

def signup(request):
	if request.method == 'POST':
	    username = request.POST['username']
	    email = request.POST['email']
	    password = request.POST['password']

	    if User.objects.filter(username=username).exists():
	    	messages.error(request, 'Username sudah ada!')
	    elif User.objects.filter(email=email).exists():
	    	messages.error(request, 'Email sudah ada!')
	    else:
	    	user = User.objects.create_user(username, email, password)
	    	return redirect('/accounts/login')

	return render(request, 'signup.html')

def logout(request):
	auth_logout(request)
	return redirect('/')
