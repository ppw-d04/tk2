from django.urls import path

from . import views

app_name = 'user'

urlpatterns = [
	path('accounts/login/', views.login, name='login'),
	path('signup/', views.signup, name='signup'),
	path('logout/', views.logout, name='logout')
]