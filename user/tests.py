from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User

class UnitTest(TestCase):

	def test_signup_url_exists(self):
		response = Client().get('/signup/')
		self.assertEqual(response.status_code, 200)

	def test_signup_template_exist(self):
		response = Client().get('/signup/')
		self.assertTemplateUsed(response, 'signup.html')

	def test_login_url_exists(self):
		response = Client().get('/accounts/login/')
		self.assertEqual(response.status_code, 200)

	def test_login_template_exist(self):
		response = Client().get('/accounts/login/')
		self.assertTemplateUsed(response, 'login.html')

	def test_logout_url_exists(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 302)

	def test_create_model(self):
		User.objects.create_user(username="admin", email="admin@gmail.com", password="admin")
		self.assertEqual(User.objects.all().count(), 1)



